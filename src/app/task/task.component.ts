import {Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {TaskModel} from '../task.model';
import {TaskService} from '../task.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit, OnDestroy {

  @Input() task: TaskModel;
  @Output() backToList = new EventEmitter<any>();
  @ViewChild('editTaskInput') editTaskInput: ElementRef;

  editTask = false;
  editTaskValue = '';
  sub: Subscription;

  constructor(private taskService: TaskService) { }

  ngOnInit() {
    this.editTaskInput.nativeElement.focus();
  }

  onBackToList() {
    this.backToList.emit();
  }

  onEditTaskName() {
    this.editTask = true;
    this.editTaskValue = this.task.name;
    window.setTimeout(() => {
      this.editTaskInput.nativeElement.focus();
    }, 0);
  }

  onBlurTaskInput() {
    if (this.editTask) {
      this.editTask = false;
      const taskName = this.editTaskInput.nativeElement.value;
      if (this.editTaskValue !== taskName && taskName !== '' && taskName !== ' ') {
        this.task.name = taskName;
        this.sub = this.taskService.editTask(this.task)
          .subscribe((data) => console.log(data));
      }
      this.editTaskValue = '';
    }
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

}
