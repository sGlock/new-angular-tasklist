import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {TaskService} from './task.service';
import {TaskModel} from './task.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  sub: Subscription;
  tasks: TaskModel[] = [];
  task: TaskModel;
  tasksIsLoading = false;
  showTask = false;

  constructor(private taskService: TaskService) {}

  ngOnInit() {
    this.sub = this.taskService.getJSON()
      .subscribe((data: TaskModel[]) => {
        this.tasks = data.filter((task) => {
          return task.obj_status === 'active';
        });
        this.tasksIsLoading = true;
      });
  }

  onShowTask(task) {
    this.task = task;
    this.showTask = true;
  }

  backToList() {
    this.showTask = false;
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
}
